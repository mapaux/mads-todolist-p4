## Corrección de la práctica

AGULLO SANCHEZ, JOSE VICENTE

[Bitbucket](https://bitbucket.org/mapaux/mads-todolist-p4)

### Documentación

Bien, pero no hacía falta poner el código fuente de *todos* los tests. Se pedía una explicación técnica de los aspectos *más importantes* de los tests realizados. Falta la explicación técnica que detalle el funcionamiento de aspectos de spec2.

### Código e implementación

Bien los datos de prueba y todos los tests (modelo y controlador). Muy bien la variedad de tests de todas las funciones del controlador.

### Calificación

Bastante bien, ¡enhorabuena!

Calificación: **0,6**

7 de enero de 2014  
Domingo Gallardo

------

#José Vicente Agulló Sánchez
#Explicación técnica

En esta práctica vamos a llevar a cabo el paso de test de manera automatizada. Para ello debemos tenerlos bien diferenciados, tanto por tipo (modelo o aplicación), como por funcionalidades a comprobar (no es lo mismo revisar el acceso a una página concreta que el hecho de tratar de hacer un POST con datos erróneos).
Posteriormente, todos los tests deben estar en la carpeta “test”, en la cual Play buscará cuando ejecutemos el comando “play test”

#Explicación del desarrollo

Para empezar, tenemos dos aspectos muy diferentes que comprobar, que son el apartado del modelo (realizar acciones sobre objetos) y el de aplicación (realizar peticiones GET o POST, mandando los datos y recibiendo una respuesta a nuestra petición). Por todo esto, tenemos los test divididos en dos archivos, “ModelSpec.scala” y “ApplicationSpec.scala”, y en esta explicación también vamos a tratarlos por separado.

Algunos tests contienen parte de la explicación dentro del código, a modo de comentario

##ModelSpec

En este apartado comprobamos las funcionalidades tratando sobre los elementos directamente, es decir, creando usuarios o editando peticiones.

Al ser unas funciones limitadas, puesto que no había una gran variedad y que unas se prueban dentro de otras, el listado de pruebas que debe pasar el modelo es el siguiente:

    create users
    update users
    delete users
    create tasks
    update tasks
    delete tasks
    check owners
    check returned tasks
    return all tasks
    Normal order
    Ascending order

###Create user

En esta prueba comprobamos que creamos un usuario, con su usuario y contraseña, ya que es un método empleado a la hora de registrar un nuevo usuario.
```
"create users" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {        
        //se crea el usuario
        User.create(User("ey", "1234"))
        
        //Se realiza la busqueda del usuario recien creado
        val Some(user) = User.findById("ey")
      
        //Al estar creado, los valores encontrados deben ser los que acabamos de asignar
        user.email must equalTo("ey")
        user.password must equalTo("1234")        
    }
 }
```
 
Esta prueba lo que hace el crear un nuevo usuario con unos valores asignados por nosotros, para más tarde buscar ese usuario y comprobar que los datos sean los correctos. Podemos apreciar que en esta prueba, a parte de la creación de usuarios, también comprobamos el método de búsqueda.



###Update users

En esta prueba, cogemos un usuario ya creado y cuyos datos conocemos, para tratar de modificarlos y asignarles nuevos valores.
```
"update users" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {        
        //Se realiza la actualización del usuario
        User.update("josevi", User("josevi", "4321"))

        //Se busca el usuario editado y se almacena en una variable
        val Some(userFinal) = User.findById("josevi")
        
        //Se comprueba que los datos han sido correctamente modificados y almacenados
        userFinal.email must equalTo("josevi")
        userFinal.password must equalTo("4321")        
      }
}
```
En este test cogemos un usuario de la base de datos, el cual se identifica por su email (nombre en este caso), y asignamos un nuevo usuario con los valores que nosotros queramos. Posteriormente buscamos el usuario que hemos editado y comprobamos que el valor que hemos decidido actualizar (la contraseña en el ejemplo) está actualizada.

###Delete user

Aquí se trata de coger un usuario ya registrado y eliminarlo completamente de nuestra base de datos. Al no estar implementado un borrado en cascada, en los tests solamente podremos comprobar los usuarios que no tengan ninguna tarea asignada.
```
"delete users" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        //Se borra el usuario
        User.delete("sinTarea")

        //Se intenta encontrar el usuario recientemente borrado
        val userFinal = User.findById("sinTarea")
      
        //El resultado de la búsqueda debe ser vacio
        userFinal must equalTo(None)        
    }
}
```

Como podemos apreciar en el test, ejecutamos la opción de borrado sobre un usuario, lo cual debería hacer que no encontráramos ningún valor al realizar su búsqueda.

En la práctica no están implementados aún los servicios de edición y borrado de usuarios, pero se incluyen estas funcionalidades debido a que por normal general las clases contienen un constructor, un editor y un destructor, además de por temas de escalabilidad, ya que en futuras versiones puede requerirse implementar un panel de administración que lleve a cabo estas labores.

###Create task

En esta prueba, al igual que en la creación de usuarios, tratamos de crear un nuevo objeto que añadir a nuestra base de datos y asignarle valores a sus atributos.
```
"create tasks" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {  
        //Se crea una tarea nueva
        Task.create(Task(anorm.NotAssigned, "Limpiar el escritorio", None), "josevi")
        
        //Se busca esta tarea recien creada y su dueño recien asignado
        val Some(task) = Task.findById(4)
        val Some(dueño) = Task.devuelveOwner(4)

        //El resultado debe ser el mismo que hemos insertado al inicio
        task.label must equalTo("Limpiar el escritorio")
        dueño must equalTo("josevi")
      }
  }
```

En esta prueba creamos una tarea al inicio, a la cual hay que pasarle los datos de la tarea y un usuario, el cual va a ser su propietario. A parte de esto, podemos apreciar que la tarea posee un elemento cuyo valor es anorm.NotAssigned, el cual es el id, y que al autoincrementarse no debe recibir ningún valor adicional, pero debemos reflejarlo. El nombre de usuario debe ser uno ya insertado en la base de datos, puesto que es clave ajena a la tabla Users y estas comprobaciones no debemos realizarlas aquí.

###Update tasks

Una de las funcionalidades requeridas casi desde un principio fue la edición de tareas, puesto que es un punto de vital importancia, ya sea para modificar una fecha o para cambiar el nombre de una tarea.
```
"update tasks" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {       
        //Se actualiza una tarea ya existente
        Task.update(2, Task(anorm.Id(2), "cambio", None))

        //Se busca la tarea por su id
        val Some(task) = Task.findById(2)
        val Some(dueño) = Task.devuelveOwner(2)

        //Se comprueba que los datos se han editado correctamente
        task.label must equalTo("cambio")
        task.tiempo must equalTo(None) 
        dueño must equalTo("potato")       
      }
 }
```

En la prueba que tenemos arriba podemos comprobar que actualizamos una tarea ya creada quitando el apartado de la fecha y editando la descripción. Tras esto, se realiza una búsqueda de la tarea mediante su id, aparte de buscar el dueño, para comprobar que este no ha sido modificado, además de comprobar que los cambios se han producido satisfactoriamente. El propietario no puede ser modificado, puesto que cada usuario puede editar sus propias tareas, y no sería correcto que un usuario pudiera asignar sus tareas a otros.

###Delete tasks

Esta prueba consiste, al igual que en el apartado de usuarios, de eliminar una tarea de nuestra base de datos.
```
"delete tasks" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {              
        //Se borra una tarea
        Task.delete(1)

        //Se busca la tarea
        val task = Task.findById(1)
      
        //El resultado de esta búsqueda debe ser vacio
        task must equalTo(None)        
      }
  }
```

Una vez eliminada la tarea, no debemos poder encontrarla mediante nuestro sistema de búsqueda.

###Check owners

En este apartado del test comprobamos el usuario correspondiente a una tarea, lo cual nos permite corroborar el dueño a la hora de hacer un listado.
```
"check owners" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {              
        
        //Se comprueba si el dueño de la tarea 2 (potato) y de la tarea 1 (josevi) son igual a "potato"
        val dueño = Task.isOwner(2, "potato")
        val dueño2 = Task.isOwner(1, "potato")

        //La primera comprobación debe devolver true y la segunda false
        dueño must equalTo(true)        
        dueño2 must equalTo(false)        
      }
}
```
En la prueba realizamos dos comprobaciones, una correcta y otra incorrecta, en tareas diferentes, con la finalidad de verificar que el método funciona.

###Check returned tasks

Esta prueba tiene su utilidad a la hora de que nos devuelvan un listado de tareas correspondientes a un usuario, puesto que debemos asegurarnos de que sólo se muestren las tareas propias.
```
"check returned tasks" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {              
        //Se almacenan todas las tareas de "josevi" en un listado
        val listado = Task.ownTasks("josevi", None)        

        //Se recorre el listado mediante un bucle, y para cada tarea se comprueba que el propietario sea "josevi"
        for (i <- 0 until listado.length) {
          val Some(dueño) = Task.devuelveOwner2(listado(i).id)
          dueño must equalTo("josevi")           
        }            

      }
}
```

En la prueba creamos un listado con las tareas de un usuario y realizamos un bucle que las recorra todas, con la finalidad de que no se haya colado ninguna incorrecta.

###Return all tasks

Tras comprobar que podemos devolver las tareas propias de un usuario, podemos también querer devolver todas las tareas almacenadas en nuestra base de datos, por lo que un método debe permitirnos esta función.
```
"return all tasks" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {              
        //Se almacena en un listado todas las tareas de la base de datos
        val listado = Task.all(None)        
        
        //Se comprueba que el tamaño del listado sea igual que el número de tareas insertadas desde la evolución 3.sql
        listado.length must equalTo(3)                             
      }
}
```

En el test devolvemos todas las tareas y comparamos la cantidad de resultados con los elementos que tenemos nosotros en el script de entrada.

###Normal order
&
###Ascending order
```
"Normal order" in {
          running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
            // Se crea una lista
            val listaSinOrden: List[Task] = Task.ownTasks("josevi", None)
            val t: Task = listaSinOrden.head
            // Se comprueba el valor de la primera tarea de la lista por orden de insercion
            t.label must equalTo("Acabar la practica")
            dateIs(t.tiempo.get, "2013-12-25") must beTrue
            
          }
        }
```
```
"Ascending order" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        // Se crea una lista        
        val listaNormal: List[Task] = Task.ownTasks("josevi", Some(1))
        val t: Task = listaNormal.head        
        val Some(dueño) = Task.devuelveOwner2(t.id)
            
        // Se comprueba el valor de la primera tarea de la lista que esta ordenada por fecha
        t.label must equalTo("Acabar la carrera")
        dateIs(t.tiempo.get, "2012-12-25") must beTrue
        dueño must equalTo("josevi")
      }
}
```

El usuario puede querer ver su lista de tareas ordenada o por defecto, por lo que comprobamos las dos opciones. En el método normal devolvemos la lista de tareas de un usuario y el primer elemento debe coincidir con el primer elemento insertado en el script correspondiente a ese usuario, mientras que en el caso de aplicar un orden, el primer elemento debe ser el que tenga una fecha menor.

##ApplicationSpec

En este otro apartado comprobamos temas de navegación, tales como redireccionamientos, elementos de la interfaz o respuestas a nuestras peticiones.

En la batería de tests disponible se han revisado todas las peticiones GET y POST a las diferentes direcciones de nuestra aplicación.

El listado de pruebas es el siguiente:

    GET send 404 on a bad request
    GET show the index page at the begining
    GET process correct redirect to login
    POST process correct login
    POST process incorrect login
    GET process correct logout
    GET process correct redirect to registrarse
    POST process correct do registry
    POST process incorrect do registry without user
    POST process incorrect do registry without password
    POST process incorrect do registry without password and user
    GET process correct security main page without login
    GET process correct security main page with login
    GET process correct security updateTask without login
    GET process correct security updateTask with login (incorrect user)
    GET process correct security updateTask with login
    POST revise updateTask whithout session
    POST revise the correct owner at updateTask whith session
    POST revise the correct owner (incorrect) at updateTask whith session
    POST revise the correct owner at updateTask whith session and bad arguments
    GET process correct redirect to new task without login
    GET process correct redirect to new task with login
    POST process correct new task without login
    POST process correct new task with login
    POST process correct new task (incorrect) with login
    POST process correct delete without login
    POST process correct delete with bad user
    POST process correct delete with correct user


###Show the index page at the begining

Al acceder a la aplicación, la página que debe mostrar es la inicial.
```
"GET show the index page at the begining" in {
    running(FakeApplication()){
  		
        //Se lanza una petición GET para acceder a "/"  
        val Some(home) = route(FakeRequest(GET, "/"))

        //La respuesta debe ser "OK", la página de tipo HTML y no contener las tres palabras indicadas
  		status(home) must equalTo(OK)
  		contentType(home) must beSome.which(_ == "text/html")
  		contentAsString(home) must not contain("Hola")
  		contentAsString(home) must not contain("seguro")
  		contentAsString(home) must not contain("Regístrate")
  	}
  }
```

En este test, comprobamos que al realizar la petición GET nos devuelve un OK como respuesta, aparte de no contener la página ciertos elementos característicos de las otras y que la hacen única.

###GET process correct redirect to login

Una vez en la página, el usuario debe acceder para registrarse, por lo que necesita acceder a la página que le permita realizar el login.
```
"GET process correct redirect to login" in {
        running(FakeApplication()) {
        //Se lanza una petición GET para acceder a "/login"
        val Some(result) = route (FakeRequest( GET, "/login"))
        
        //Se debe recibirun "OK" como respuesta, la página ha de ser de tipo HTML y contener la palabra indicada
        status(result) must equalTo(OK)
        contentType(result) must beSome.which(_ == "text/html")
        contentAsString(result) must contain("seguro")
      }
    }
```

Se puede apreciar que en este test se realiza una petición a la página de login, y que la respuesta recibida es un OK y que la página contiene un elemento que la distingue del resto.

###POST process correct login
&
###POST process incorrect login
```
"POST process correct login" in {
      running(FakeApplication()) {
        
        //Se simula una petición POST a la página "/login" con un formulario con apartados email y password relleno con valores correctos
        val Some(result) = route(
          FakeRequest(POST, "/login").withFormUrlEncodedBody(("email","josevi"),("password","patata"))
        )
      
        //La respuesta debe ser una redirección al índice, además de haber creado una sesión
        status(result) must equalTo(SEE_OTHER)
        redirectLocation(result) must equalTo(Some("/index"))
        session(result).apply("email") must equalTo("josevi")
      }      
    }
```
```

    "POST process incorrect login" in {
      running(FakeApplication()) {
        
        //Se simula una petición POST a la página "/login" con un formulario con apartados email y password relleno con valores incorrectos
        val Some(result) = route(
          FakeRequest(POST, "/login").withFormUrlEncodedBody(("email","josevi"),("password","juasjuasjuas"))
        )

        //La respuesta debe ser una redirección a la parte pública y no se debe crear sesión, ya que los datos no son válidos
        status(result) must equalTo(SEE_OTHER)
        redirectLocation(result) must equalTo(Some("/"))
      }      
    }
```

En lo referente a la petición POST a la página de login, tenemos dos opciones, que el login sea correcto o no. En caso de que el login sea correcto, recibiremos como respuesta un SEE_OTHER, lo cual indica que vamos aser redirigidos a otra página, siendo en este caso la pantalla principal del usuario, además de haberse creado una sesión con el email del usuario. En caso contrario, se recibirá también un SEE_OTHER, pero en este caso a la página inicial de la aplicación, donde se puede acceder al login o al registro, aparte de que no se habrá iniciado sesión alguna.

###GET process correct logout

Un usuario no debe o puede no querer estar permanentemente conectado, por lo que se le debe ofrecer una opción de cerrar sesión.
```
"GET process correct logout" in {
      running(FakeApplication()) {
        //Se lanza una petición GET a la página "/logout"
	    val Some(result) = route (FakeRequest( GET, "/logout"))
	    
        //La cabecera devuelta debe indicar que la cookie ha expirado
        headers(result).get("Set-Cookie").getOrElse("") must contain("Expires")
	}  
}
```

En el test comprobamos que al realizar una petición GET para llevar a cabo este cierre de sesión, la cabecera de respuesta nos muestre que la sesión expira para confirmar que se cierra la sesión.

###GET process correct redirect to registrarse

En toda aplicación con parte privada se debe permitir a un usuario crearse una cuenta, la cual le brindará acceso a las funcionalidades de la aplicación.
```
"GET process correct redirect to registrarse" in {
      running(FakeApplication()) {
      	
        //Se realiza una petición GET a la página "/registrarse"  
        val Some(result) = route (FakeRequest( GET, "/registrarse"))
        
        //Se debe recibir como respuesta un "OK" y una redirección a la página, que sea de tipo HTML y que contenga la frase indicada
	    status(result) must equalTo(OK)
	    contentType(result) must beSome.which(_ == "text/html")
  	    contentAsString(result) must contain("Regístrate")
	}
  }
```

En esta prueba comprobamos que el acceso al formulario de registro sea correcto, por lo que tras realizar una petición GET, la respuesta de nuestra aplicación debe ser un OK, a parte de que en la página se nos mostrará un texto identificativo indicando qué debemos hacer.

###POST process correct do registry
```
"POST process correct do registry" in {
      running(FakeApplication()) {
        
        //Se simula una petición POST a la página "/registrarse" con un formulario con apartados email y password relleno con valores correctos
        val Some(result) = route (FakeRequest( POST, "/registrarse").withFormUrlEncodedBody(("email","Patata"),("password","contraseña")))
        
        //Se debe recibir como respuesta una redirección al índice y se debe haber creado una sesión con el usuario registrado
        status(result) must equalTo(SEE_OTHER)    
        redirectLocation(result) must equalTo(Some("/index")) 
        session(result).apply("email") must equalTo("Patata")
      }
    }    
```

###POST process incorrect do registry without user
&
###POST process incorrect do registry without password
&
###POST process incorrect do registry without password and user
```
   "POST process incorrect do registry without user" in {
      running(FakeApplication()) {
        //Se simula una petición POST a la página "/registrarse" con un formulario con apartados email y password, estando el usuario vacio
        val Some(result) = route (FakeRequest( POST, "/registrarse").withFormUrlEncodedBody(("email",""),("password","contraseña")))

        //Se debe recibir un BAD_REQUEST
        status(result) must equalTo(BAD_REQUEST)            
      }
    }    
```
```
    "POST process incorrect do registry without password" in {
      running(FakeApplication()) {
        //Se simula una petición POST a la página "/registrarse" con un formulario con apartados email y password, estando el password vacio
        val Some(result) = route (FakeRequest( POST, "/registrarse").withFormUrlEncodedBody(("email","Patata"),("password","")))

        //Se debe recibir un BAD_REQUEST
        status(result) must equalTo(BAD_REQUEST)    
      }
    }
```
```
    "POST process incorrect do registry without password and user" in {
      running(FakeApplication()) {
        //Se simula una petición POST a la página "/registrarse" con un formulario con apartados email y password, estando el usuario y el password vacios
        val Some(result) = route (FakeRequest( POST, "/registrarse").withFormUrlEncodedBody(("email",""),("password","")))

        //Se debe recibir un BAD_REQUEST
        status(result) must equalTo(BAD_REQUEST)    
      }
    }
```

En cuanto a la petición POST referente a la página de registro existen dos posibilidades. La primera y más sencilla consiste en que los datos de los dos apartados del formulario sean correctos, por lo que se recibe como respuesta un SEE_OTHER, el cual indica que se nos va a redireccionar, en este caso a la página principal del usuario, además de haberse creado una sesión con el usuario recién registrado.
Por otro lado tenemos la opción de un error, que puede ser debido a que falte el campo del email, el campo de la contraseña o ambos. En estos tres casos la respuesta debe ser un BAD_REQUEST y que se nos muestre en pantalla el error.

###GET process correct security main page without login
&
###GET process correct security main page with login
```
"GET process correct security main page without login" in {
      running(FakeApplication()) {
        
        //Se lanza una petición GET a "/tasks" sin una sesión creada
        val Some(result) = route(FakeRequest(GET, "/tasks"))
    
        //Se debe recibir como respuesta una redirección a la parte pública
        redirectLocation(result) must equalTo(Some("/"))          
      
      }      
    }
```
```
"GET process correct security main page with login" in {
      running(FakeApplication()) {

        //Se realiza una petición GET a "/tasks" indicando que hay una sesión iniciada y que el "email" es "josevi"
        val Some(result) = route(FakeRequest(GET, "/tasks").withSession("email" -> "josevi"))
    
        //Se debe mostrar por pantalla la página de tasks, que es de tipo HTML y contiene unas palabras concretas
        contentType(result) must beSome.which(_ == "text/html")
        contentAsString(result) must contain("Hola") 
        contentAsString(result) must contain("Add a new task")   
      
      }      
    }
```
Tras comprobar que un usuario es capaz de registrarse y de loguearse, el siguiente punto a comprobar es el acceso a la página principal del usuario, en el cual se le muestran sus tareas. Al ser una parte privada, un usuario que no ha iniciado sesión no debe poder acceder, por lo que ante esta petición GET existen dos posibles respuestas, una redirección a la página inicial pública en el caso de que no esté logueado el usuario, o bien un OK y ver cómo se muestra por pantalla la página principal del usuario logueado en caso de que existiera una sesión iniciada.

###GET process correct redirect to new task without login
&
###GET process correct redirect to new task with login
```
"GET process correct redirect to new task without login" in {
    running(FakeApplication()) {

      val Some(result) = route(
        FakeRequest(GET, "/tasks/new")
        )

      status(result) must equalTo(SEE_OTHER)
      redirectLocation(result) must equalTo(Some("/"))        
    }      
  }
```
```
  "GET process correct redirect to new task with login" in {
    running(FakeApplication()) {
      val Some(result) = route(
        FakeRequest(GET, "/tasks/new").withSession("email" -> "potato")
        )

      status(result) must equalTo(OK)     
    }      
  }
```

Al igual que en el apartado anterior, cuando un usuario desea acceder a la página de creación de tareas mediante una petición GET, tiene dos respuestas posibles, dependiendo de si está logueado o no. En caso de estar logueado, se debe recibir un OK, lo cual indica que se ha cargado la página solicitada, mientras que en caso contrario se recibe un SEE_OTHER que redirecciona a la página principal de la parte pública.

###POST process correct new task without login
&
###POST process correct new task with login
&
###POST process correct new task (incorrect) with login
```
"POST process correct new task without login" in {
    running(FakeApplication()) {

      val Some(result) = route(
        FakeRequest(POST, "/tasks").withFormUrlEncodedBody(("label","probatura"),("tiempo","25-12-2013"))
      )

      status(result) must equalTo(SEE_OTHER)
      redirectLocation(result) must equalTo(Some("/"))        
    }      
  }
```
```
  "POST process correct new task with login" in {
    running(FakeApplication()) {

      val Some(result) = route(
        FakeRequest(POST, "/tasks").withFormUrlEncodedBody(("label","probatura"),("tiempo","25-12-2013")).withSession("email" -> "potato")
      )

      status(result) must equalTo(SEE_OTHER)
      redirectLocation(result) must equalTo(Some("/tasks"))        
    }      
  }
```
```
  "POST process correct new task (incorrect) with login" in {
    running(FakeApplication()) {

      val Some(result) = route(
        FakeRequest(POST, "/tasks").withFormUrlEncodedBody(("label",""),("tiempo","25-12-2013")).withSession("email" -> "potato")
      )

      status(result) must equalTo(BAD_REQUEST)
      
    }      
  }
```

Tras acceder a la página de creación de tareas, se debe realizar una petición POST para almacenar esta nueva tarea en la base de datos, con sus consiguientes posibilidades de devolver una respuesta indicando que sí ha aceptado o que no, tal y como ha pasado con el registro y veremos más adelante con otros apartados. En caso de estar logueados y que los campos tengan valores correctos, el usuario recibe como respuesta un SEE_OTHER que le redirecciona a su página principal. En cambio, si no está logueado será redireccionado a la parte pública, o si ha iniciado sesión pero no ha insertado un nombre para la tarea, recibirá un BAD_REQUEST

###GET process correct security updateTask without login
&
###GET process correct security updateTask with login (incorrect user)
&
###GET process correct security updateTask with login
```
"GET process correct security updateTask without login" in {
      running(FakeApplication()) {

        val Some(result) = route(FakeRequest(GET, "/tasks/1"))
    
        status(result) must equalTo(SEE_OTHER)
        redirectLocation(result) must equalTo(Some("/")) 
      
      }      
    }
```
```
    "GET process correct security updateTask with login (incorrect user)" in {
      running(FakeApplication()) {

        val Some(result) = route(FakeRequest(GET, "/tasks/1").withSession("email" -> "potato"))
    
        status(result) must equalTo(FORBIDDEN)      
      }      
    }
```
```
    "GET process correct security updateTask with login" in {
      running(FakeApplication()) {

        val Some(result) = route(FakeRequest(GET, "/tasks/1").withSession("email" -> "josevi"))
    
        status(result) must equalTo(OK)      
      }      
    }
```

Una vez puede crear tareas, el usuario tiene la posibilidad de editarlas, ya sea para cambiar el nombre o la fecha, por lo que debe acceder a la página con el formulario correspondiente a través de una petición GET. Al igual que en los casos anteriores, si el usuario no está logueado, se le redireccionará a la parte pública, mientras que si está logueado aparecen ahora dos opciones más. Si el usuario logueado es el propietario de la tarea a la que se quiere acceder, recibirá un OK y podrá ver la página, mientras que si el usuario logueado no es el propietario, recibirá como respuesta un FORBIDDEN.

###POST revise updateTask whithout session
&
###POST revise the correct owner at updateTask whith session
&
###POST revise the correct owner (incorrect) at updateTask whith session
&
###POST revise the correct owner at updateTask whith session and bad arguments
```
"POST revise updateTask whithout session" in {      
      running(FakeApplication()) {

        val Some(result) = route(
          FakeRequest(POST, "/tasks/2").withFormUrlEncodedBody(("id", "2"),("label","Patata"),("tiempo","1991-12-26")))
    
        redirectLocation(result) must equalTo(Some("/"))          
      
      }      
    }
```
```
    "POST revise the correct owner at updateTask whith session" in {
      running(FakeApplication()) {

        val Some(result) = route(
          FakeRequest(POST, "/tasks/2").withFormUrlEncodedBody(("id", "2"),("label","Patata"),("tiempo","1991-12-26")).withSession("email" -> "potato")
        )

        status(result) must equalTo(SEE_OTHER)    
        redirectLocation(result) must equalTo(Some("/tasks"))          
      
      }      
    }
```
```
    "POST revise the correct owner (incorrect) at updateTask whith session" in {
      running(FakeApplication()) {

        val Some(result) = route(
          FakeRequest(POST, "/tasks/2").withFormUrlEncodedBody(("id", "2"),("label","Patata"),("tiempo","1991-12-26")).withSession("email" -> "josevi")
        )

        status(result) must equalTo(FORBIDDEN)          
      }      
    }
```
```
    "POST revise the correct owner at updateTask whith session and bad arguments" in {
      running(FakeApplication()) {

        val Some(result) = route(
          FakeRequest(POST, "/tasks/2").withFormUrlEncodedBody(("id", "2"),("label",""),("tiempo","1991-12-26")).withSession("email" -> "potato")
        )

        status(result) must equalTo(BAD_REQUEST)                  
      }      
    }
```
Una vez se ha accedido a la página, se procede a guardar los cambios mediante un método POST. Al igual que antes, si el usuario que trata de llevar a cabo esto no está logueado, será redirigido a la parte pública, mientras que si está logueado hay varias opciones a llevar a cabo.
Si el usuario logueado no es el dueño de la tarea, recibirá como respuesta un FORBIDDEN, lo cual le indica que tiene prohibido llevar a cabo la edición. En cambio, si es el dueño, está la opción de que los parámetros sean correctos, por lo que se recibirá una redirección a la página principal privada, o que los parámetros sean incorrectos (tarea sin nombre), con lo que la respuesta recibida será un BAD_REQUEST

###POST process correct delete without login
&
###POST process correct delete with bad user
&
###POST process correct delete with correct user
```
"POST process correct delete without login" in {
    running(FakeApplication()) {

      val Some(result) = route(FakeRequest(POST, "/tasks/2/delete"))

        status(result) must equalTo(SEE_OTHER)
        redirectLocation(result) must equalTo(Some("/"))      
    }
  }      
```
```
  "POST process correct delete with bad user" in {
    running(FakeApplication()) {

      val Some(result) = route(FakeRequest(POST, "/tasks/2/delete").withSession("email" -> "josevi"))

      status(result) must equalTo(FORBIDDEN) 
      
    }
  }      
```
```
  "POST process correct delete with correct user" in {
    running(FakeApplication()) {

      val Some(result) = route(FakeRequest(POST, "/tasks/2/delete").withSession("email" -> "potato"))
      
      status(result) must equalTo(SEE_OTHER)
      redirectLocation(result) must equalTo(Some("/tasks"))        
    }
  }
```

Por último de las funcionalidades implementadas (que vaya a emplear el usuario, puesto que la autenticación es posterior pero el usuario no interacciona directamente con ella), y por tanto testeadas, encontramos la opción de borrar tareas, la cual puede ser realizada solo por el propietario. Una vez más, si un usuario anónimo trata de llevar a cabo uno de estos métodos POST, se le redirigirá a la página pública, mientras que si es un usuario logueado se comprobará que sea el dueño. En caso afirmativo se procederá a borrar la tarea y a redireccionar a la página principal, mientras que en caso contrario recibirá como respuesta un FORBIDDEN indicando que tiene prohibido realizar esa acción sobre esa tarea.