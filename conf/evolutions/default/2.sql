# Tasks schema
 
# --- !Ups

CREATE TABLE user (
    email varchar(255) PRIMARY KEY,
    password varchar(255) NOT NULL
);

ALTER TABLE task ADD propietario varchar(255);
ALTER TABLE task ADD FOREIGN KEY (propietario) REFERENCES user(email);
 
# --- !Downs
 
DROP TABLE task;

