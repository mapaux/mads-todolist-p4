# Insert Datos 
# --- !Ups
insert into user (email, password) values ('josevi', 'patata');
insert into user (email, password) values ('potato', 'patatuela');
insert into user (email, password) values ('sinTarea', 'patata');

insert into task (label, tiempo, propietario) values ('Acabar la practica', '2013-12-25', 'josevi');
insert into task (label, tiempo, propietario) values ('Entregar todo', '1991-12-25', 'potato');
insert into task (label, tiempo, propietario) values ('Acabar la carrera', '2012-12-25', 'josevi');

# --- !Downs
delete from user;
delete from task;