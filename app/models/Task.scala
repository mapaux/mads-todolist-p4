package models

import anorm._
import anorm.SqlParser._

import java.util.{Date}

import play.api.db._
import play.api.Play.current

case class Task(id: Pk[Long], label: String, tiempo: Option[Date])

object Task {

  val task = {
    get[Pk[Long]]("id") ~ 
    get[String]("label") ~
    get[Option[Date]]("tiempo") map {
      case id~label~tiempo => Task(id, label,tiempo)
    }
  }

  def create(task: Task, username: String) {
    DB.withConnection {
      implicit c => SQL("insert into task (label, propietario, tiempo) values ({label},({propietario}), {tiempo})"
        ).on(
        'label -> task.label,
        'propietario-> username, 
        'tiempo -> task.tiempo
        ).executeUpdate()
    }
  }
  
  def delete(id: Long) {
    DB.withConnection{ implicit c => 
      SQL("delete from task where id = {id}").on(
        'id -> id
        ).executeUpdate()
    }
  }

  def update(id: Long, task: Task){
    DB.withConnection{ implicit c => 
      SQL("update task set label = {label}, tiempo = {tiempo} where id = {id}").on(
        'id -> id, 
        'label -> task.label, 
        'tiempo -> task.tiempo
        ).executeUpdate()
    }
  }
  
 def all(orden: Option[Int] = None): List[Task] = 
  orden match{
      case Some(orden) => DB.withConnection { implicit c => SQL("select * from task order by tiempo nulls last").as(task *)}
      case None => DB.withConnection { implicit c => SQL("select * from task").as(task *)}
  }

  def findById(id: Long): Option[Task] ={
    DB.withConnection{
      implicit c => SQL("select * from task where id = {id}").on('id -> id).as(Task.task.singleOpt)
    }
  }

  def devuelveOwner(id: Long): Option[String] ={
    DB.withConnection{
      implicit c => SQL("select propietario from task where id = {id}").on('id -> id).as(scalar[String].singleOpt)
    }
  }

  def devuelveOwner2(id: Pk[Long]): Option[String] ={
    DB.withConnection{
      implicit c => SQL("select propietario from task where id = {id}").on('id -> id).as(scalar[String].singleOpt)
    }
  }

  def ownTasks(user:String, orden: Option[Int]): List[Task] = 
  DB.withConnection { implicit connection =>
      orden match {
        case Some(orden) => 
          SQL("select * from task where task.propietario = {email} order by tiempo nulls last"
            ).on(
            'email -> user
            ).as(task *)
        case None => 
          SQL(" select * from task where task.propietario = {email} order by id"
            ).on(
            'email -> user
            ).as(task *)  
      }      
  }

  def isOwner(id: Long, user: String): Boolean = {
    DB.withConnection { implicit connection =>
      SQL(
        """
          select count(task.id) = 1 from task 
          where task.propietario = {email} and task.id = {id}
        """
      ).on(
        'id -> id,
        'email -> user
      ).as(scalar[Boolean].single)
    }
  }
}
