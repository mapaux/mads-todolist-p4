package models

import anorm._
import anorm.SqlParser._

import java.util.{Date}

import play.api.db._
import play.api.Play.current

case class User(email: String, password: String)

object User {

  def all(): List[User] = 
      DB.withConnection { implicit c => 
        SQL("select * from user").as(user *)
    }

    def create(label: User) {
    DB.withConnection { implicit c =>
      SQL("insert into user (email, password) values ({email}, {password})").on(
        'email -> label.email,
        'password -> label.password
      ).executeUpdate()
    }
  }

  def update(email: String, user: User) = {
    DB.withConnection { implicit connection =>
      SQL(
        "update user set email = {email}, password = {password} where email = {email}"
      ).on(
        'email -> email,
        'password -> user.password
      ).executeUpdate()
    }
  }

  def delete(email: String) {
    DB.withConnection { implicit c =>
      SQL("delete from user where email = {email}").on(
        'email -> email
      ).executeUpdate()
    }
  }

  /**
   * Retrieve a user from the id.
   */
  def findById(email: String): Option[User] = {
    DB.withConnection { implicit connection =>
      SQL("select * from user where email = {email}").on(
        'email -> email
        ).as(User.user.singleOpt)
    }
  }

  // -- Parsers
  
  /**
   * Parse a User from a ResultSet
   */
  val user = {
    get[String]("email") ~
    get[String]("password") map {
      case email~password => User(email, password)
    }
  } 
}
