package controllers

import models._
import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import anorm._

object Application extends Controller with Secured {
  
var tipoOrden : Option[Int] = None
  
def index = Action {
  Redirect(routes.Application.tasks(tipoOrden))
}

val taskForm = Form(
  mapping(
    "id" -> ignored(NotAssigned:Pk[Long]),
    "label" -> nonEmptyText,
    "fecha" -> optional(date("dd-MM-yyyy"))
  )(Task.apply)(Task.unapply)
)

val userFormReg = Form(
  mapping(
    "email" -> nonEmptyText,
    "password" -> nonEmptyText
  )(User.apply)(User.unapply)
)

def tasks(orderBy: Option[Int]) = IsAuthenticated { username => _ =>
  User.findById(username).map { user =>
  tipoOrden = orderBy
  Ok(views.html.index(Task.ownTasks(username, tipoOrden), taskForm, username))}.getOrElse(Forbidden)
}


def newTask = IsAuthenticated { username => implicit request =>
  Ok(views.html.newTask(taskForm, username))
}

def inicio =Action {
  { implicit request =>
       Ok(views.html.inicio())
  }
}


def save = IsAuthenticated { username => implicit request =>
  taskForm.bindFromRequest.fold(
    errors => BadRequest(views.html.newTask(errors, username)),
    label => {
      Task.create(label, username)
      Redirect(routes.Application.tasks(tipoOrden))
    }
  )
}

def editTask(id: Long) = IsOwnerOf(id) { username => implicit request =>
  Task.findById(id).map { task =>
      Ok(views.html.editTask(task, taskForm.fill(task), username))
    }.getOrElse(NotFound)
}

def updateTask(id: Long) = IsOwnerOf(id) { username => implicit request =>
  taskForm.bindFromRequest.fold(
      formWithErrors => { Task.findById(id) match {
        case Some(task) => BadRequest(views.html.editTask(task, formWithErrors, username))
        case None => Ok(views.html.index(Task.ownTasks(username, tipoOrden), taskForm, username))
        }
      },
      task => {
        Task.update(id, task)
        Redirect(routes.Application.tasks(tipoOrden))
      }
    )
}

def deleteTask(id: Long) = IsOwnerOf(id) { _ => implicit request =>
  Task.delete(id)
  Redirect(routes.Application.tasks(tipoOrden))
}

def newUser = Action {
  Ok(views.html.register(userFormReg))
}

def registrarse = Action { implicit request =>
  userFormReg.bindFromRequest.fold(
    errors => BadRequest(views.html.register(errors)),
    user => {
      User.findById(user.email) match {
        case Some(usuario) => {
          Redirect(routes.Application.login)        
          }
        case _ =>{ 
          User.create(user)
          Redirect(routes.Application.index()).withSession("email"->user.email)
        }
      }
    }
  )
}

def logout = Action {
  Redirect(routes.Application.inicio).withNewSession
}

def login = Action { implicit request =>
  Ok(views.html.login(userFormReg))
}

def autenticar = Action { implicit request =>
  userFormReg.bindFromRequest.fold(
    formWithErrors => BadRequest(views.html.login(formWithErrors)),
    user => { User.findById(user.email) match {
      case Some(usrFind) => if(usrFind.password == user.password){
                              Redirect(routes.Application.index()).withSession("email" -> user.email)      
                            }
                            else{
                              Redirect(routes.Application.inicio()) //añadir forbidden
                            }                                         
      case None => Redirect(routes.Application.inicio()) //añadir forbidden        
    }        
  }
  )
}
}

trait Secured {
   /**
   * Retrieve the connected user email.
   */
  private def username(request: RequestHeader) = request.session.get("email")

  /**
   * Redirect to login if the user in not authorized.
   */
  private def onUnauthorized(request: RequestHeader) = Results.Redirect(routes.Application.inicio)
  
  // --
  
  /** 
   * Action for authenticated users.
   */
  def IsAuthenticated(f: => String => Request[AnyContent] => Result) = Security.Authenticated(username, onUnauthorized) { user =>
    Action(request => f(user)(request))
  }

  /**
   * Check if the connected user is a owner of this task.
   */
  def IsOwnerOf(task: Long)(f: => String => Request[AnyContent] => Result) = IsAuthenticated { user => request =>
    if(Task.isOwner(task, user)) {
      f(user)(request)
    } else {
      Results.Forbidden
    }
  }
}
