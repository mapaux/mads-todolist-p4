package test

import org.specs2.mutable._

import play.api.test._
import play.api.test.Helpers._

class ApplicationSpec extends Specification {
  
  import controllers._

  // -- Date helpers
  
  def dateIs(date: java.util.Date, str: String) = new java.text.SimpleDateFormat("yyyy-MM-dd").format(date) == str
  
  // --
  
  "Application" should {

//URL INVENTADA

  	"GET send 404 on a bad request" in {
  		running(FakeApplication()){
  			route(FakeRequest(GET, "/patata")) must beNone
  		}
  	}

//INICIO

  	"GET show the index page at the begining" in {
  		running(FakeApplication()){
  			val Some(home) = route(FakeRequest(GET, "/"))

  			status(home) must equalTo(OK)
  			contentType(home) must beSome.which(_ == "text/html")
  			contentAsString(home) must not contain("Hola")
  			contentAsString(home) must not contain("seguro")
  			contentAsString(home) must not contain("Regístrate")
  		}
  	}

//LOGIN

    "GET process correct redirect to login" in {
        running(FakeApplication()) {

        val Some(result) = route (FakeRequest( GET, "/login"))
      
        status(result) must equalTo(OK)
        contentType(result) must beSome.which(_ == "text/html")
        contentAsString(result) must contain("seguro")
      }
    }

  	"POST process correct login" in {
      running(FakeApplication()) {

        val Some(result) = route(
          FakeRequest(POST, "/login").withFormUrlEncodedBody(("email","josevi"),("password","patata"))
        )

        status(result) must equalTo(SEE_OTHER)
        redirectLocation(result) must equalTo(Some("/index"))
        session(result).apply("email") must equalTo("josevi")
      }      
    }

    "POST process incorrect login" in {
      running(FakeApplication()) {

        val Some(result) = route(
          FakeRequest(POST, "/login").withFormUrlEncodedBody(("email","josevi"),("password","juasjuasjuas"))
        )
     
        status(result) must equalTo(SEE_OTHER)
        redirectLocation(result) must equalTo(Some("/"))
      }      
    }

//LOGOUT

    "GET process correct logout" in {
      running(FakeApplication()) {

        val Some(result) = route (FakeRequest( GET, "/logout"))
		    headers(result).get("Set-Cookie").getOrElse("") must contain("Expires")
	    }  
	  }

//REGISTRARSE

	  "GET process correct redirect to registrarse" in {
      running(FakeApplication()) {

      	val Some(result) = route (FakeRequest( GET, "/registrarse"))
		    status(result) must equalTo(OK)
    		contentType(result) must beSome.which(_ == "text/html")
  		  contentAsString(result) must contain("Regístrate")
	    }
	  }

    "POST process correct do registry" in {
      running(FakeApplication()) {

        val Some(result) = route (FakeRequest( POST, "/registrarse").withFormUrlEncodedBody(("email","Patata"),("password","contraseña")))

        status(result) must equalTo(SEE_OTHER)    
        redirectLocation(result) must equalTo(Some("/index")) 
        session(result).apply("email") must equalTo("Patata")
      }
    }    

    "POST process incorrect do registry without user" in {
      running(FakeApplication()) {

        val Some(result) = route (FakeRequest( POST, "/registrarse").withFormUrlEncodedBody(("email",""),("password","contraseña")))

        status(result) must equalTo(BAD_REQUEST)            
      }
    }    

    "POST process incorrect do registry without password" in {
      running(FakeApplication()) {

        val Some(result) = route (FakeRequest( POST, "/registrarse").withFormUrlEncodedBody(("email","Patata"),("password","")))

        status(result) must equalTo(BAD_REQUEST)    
      }
    }

    "POST process incorrect do registry without password and user" in {
      running(FakeApplication()) {

        val Some(result) = route (FakeRequest( POST, "/registrarse").withFormUrlEncodedBody(("email",""),("password","")))

        status(result) must equalTo(BAD_REQUEST)    
      }
    }    

//TASKS

	  "GET process correct security main page without login" in {
      running(FakeApplication()) {

        val Some(result) = route(FakeRequest(GET, "/tasks"))
    
        redirectLocation(result) must equalTo(Some("/"))          
      
      }      
    }

    "GET process correct security main page with login" in {
      running(FakeApplication()) {

        val Some(result) = route(FakeRequest(GET, "/tasks").withSession("email" -> "josevi"))
    
        contentType(result) must beSome.which(_ == "text/html")
        contentAsString(result) must contain("Hola") 
        contentAsString(result) must contain("Add a new task")   
      
      }      
    }

//UPDATE TASK

    "GET process correct security updateTask without login" in {
      running(FakeApplication()) {

        val Some(result) = route(FakeRequest(GET, "/tasks/1"))
    
        status(result) must equalTo(SEE_OTHER)
        redirectLocation(result) must equalTo(Some("/")) 
      
      }      
    }

    "GET process correct security updateTask with login (incorrect user)" in {
      running(FakeApplication()) {

        val Some(result) = route(FakeRequest(GET, "/tasks/1").withSession("email" -> "potato"))
    
        status(result) must equalTo(FORBIDDEN)      
      }      
    }

    "GET process correct security updateTask with login" in {
      running(FakeApplication()) {

        val Some(result) = route(FakeRequest(GET, "/tasks/1").withSession("email" -> "josevi"))
    
        status(result) must equalTo(OK)      
      }      
    }

    "POST revise updateTask whithout session" in {      
      running(FakeApplication()) {

        val Some(result) = route(
          FakeRequest(POST, "/tasks/2").withFormUrlEncodedBody(("id", "2"),("label","Patata"),("tiempo","1991-12-26")))
    
        redirectLocation(result) must equalTo(Some("/"))          
      
      }      
    }

    "POST revise the correct owner at updateTask whith session" in {
      running(FakeApplication()) {

        val Some(result) = route(
          FakeRequest(POST, "/tasks/2").withFormUrlEncodedBody(("id", "2"),("label","Patata"),("tiempo","1991-12-26")).withSession("email" -> "potato")
        )

        status(result) must equalTo(SEE_OTHER)    
        redirectLocation(result) must equalTo(Some("/tasks"))          
      
      }      
    }

    "POST revise the correct owner (incorrect) at updateTask whith session" in {
      running(FakeApplication()) {

        val Some(result) = route(
          FakeRequest(POST, "/tasks/2").withFormUrlEncodedBody(("id", "2"),("label","Patata"),("tiempo","1991-12-26")).withSession("email" -> "josevi")
        )

        status(result) must equalTo(FORBIDDEN)          
      }      
    }

    "POST revise the correct owner at updateTask whith session and bad arguments" in {
      running(FakeApplication()) {

        val Some(result) = route(
          FakeRequest(POST, "/tasks/2").withFormUrlEncodedBody(("id", "2"),("label",""),("tiempo","1991-12-26")).withSession("email" -> "potato")
        )

        status(result) must equalTo(BAD_REQUEST)                  
      }      
    }

//CREATE TASK

  "GET process correct redirect to new task without login" in {
    running(FakeApplication()) {

      val Some(result) = route(
        FakeRequest(GET, "/tasks/new")
        )

      status(result) must equalTo(SEE_OTHER)
      redirectLocation(result) must equalTo(Some("/"))        
    }      
  }

  "GET process correct redirect to new task with login" in {
    running(FakeApplication()) {

      val Some(result) = route(
        FakeRequest(GET, "/tasks/new").withSession("email" -> "potato")
        )

      status(result) must equalTo(OK)     
    }      
  }

  "POST process correct new task without login" in {
    running(FakeApplication()) {

      val Some(result) = route(
        FakeRequest(POST, "/tasks").withFormUrlEncodedBody(("label","probatura"),("tiempo","25-12-2013"))
      )

      status(result) must equalTo(SEE_OTHER)
      redirectLocation(result) must equalTo(Some("/"))        
    }      
  }

  "POST process correct new task with login" in {
    running(FakeApplication()) {

      val Some(result) = route(
        FakeRequest(POST, "/tasks").withFormUrlEncodedBody(("label","probatura"),("tiempo","25-12-2013")).withSession("email" -> "potato")
      )

      status(result) must equalTo(SEE_OTHER)
      redirectLocation(result) must equalTo(Some("/tasks"))        
    }      
  }

  "POST process correct new task (incorrect) with login" in {
    running(FakeApplication()) {

      val Some(result) = route(
        FakeRequest(POST, "/tasks").withFormUrlEncodedBody(("label",""),("tiempo","25-12-2013")).withSession("email" -> "potato")
      )

      status(result) must equalTo(BAD_REQUEST)
      
    }      
  }


//DELETE

  "POST process correct delete without login" in {
    running(FakeApplication()) {

      val Some(result) = route(FakeRequest(POST, "/tasks/2/delete"))

        status(result) must equalTo(SEE_OTHER)
        redirectLocation(result) must equalTo(Some("/"))      
    }
  }      

  "POST process correct delete with bad user" in {
    running(FakeApplication()) {

      val Some(result) = route(FakeRequest(POST, "/tasks/2/delete").withSession("email" -> "josevi"))

      status(result) must equalTo(FORBIDDEN) 
      
    }
  }      

  "POST process correct delete with correct user" in {
    running(FakeApplication()) {

      val Some(result) = route(FakeRequest(POST, "/tasks/2/delete").withSession("email" -> "potato"))
      
      status(result) must equalTo(SEE_OTHER)
      redirectLocation(result) must equalTo(Some("/tasks"))        
    }
  }        
  }
}