package test

import org.specs2.mutable._

import play.api.test._
import play.api.test.Helpers._

class ModelSpec extends Specification {
  
  import models._

  // -- Date helpers
  
  def dateIs(date: java.util.Date, str: String) = new java.text.SimpleDateFormat("yyyy-MM-dd").format(date) == str
  
  // --
  
  "Models" should {
    
    //USERS
    "create users" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        
        User.create(User("ey", "1234"))

        val Some(user) = User.findById("ey")
      
        user.email must equalTo("ey")
        user.password must equalTo("1234")        
      }
    }

    "update users" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        
        User.update("josevi", User("josevi", "4321"))

        val Some(userFinal) = User.findById("josevi")
      
        userFinal.email must equalTo("josevi")
        userFinal.password must equalTo("4321")        
      }
    }

    "delete users" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        User.delete("sinTarea")

        val userFinal = User.findById("sinTarea")
      
        userFinal must equalTo(None)        
      }
    }
      
    //TASKS
    "create tasks" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        
        Task.create(Task(anorm.NotAssigned, "Limpiar el escritorio", None), "josevi")

        val Some(task) = Task.findById(4)
        val Some(dueño) = Task.devuelveOwner(4)

        task.label must equalTo("Limpiar el escritorio")
        dueño must equalTo("josevi")
      }
    }

    "update tasks" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        
        Task.update(2, Task(anorm.Id(2), "cambio", None))

        val Some(task) = Task.findById(2)
        val Some(dueño) = Task.devuelveOwner(2)

        task.label must equalTo("cambio")
        task.tiempo must equalTo(None) 
        dueño must equalTo("potato")       
      }
    }

    "delete tasks" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {              

        Task.delete(1)

        val task = Task.findById(1)
      
        task must equalTo(None)        
      }
    }
    
    "check owners" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {              

        val dueño = Task.isOwner(2, "potato")
        val dueño2 = Task.isOwner(1, "potato")

        dueño must equalTo(true)        
        dueño2 must equalTo(false)        
      }
    }

    "check returned tasks" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {              

        val listado = Task.ownTasks("josevi", None)        

        for (i <- 0 until listado.length) {
          val Some(dueño) = Task.devuelveOwner2(listado(i).id)
          dueño must equalTo("josevi")           
        }            

      }
    }

    "return all tasks" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {              
        val listado = Task.all(None)        

        listado.length must equalTo(3)                             
      }
    }

    "Normal order" in {
          running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
            // Creo una lista
            val listaSinOrden: List[Task] = Task.ownTasks("josevi", None)
            val t: Task = listaSinOrden.head
            // Compruebo el valor de la primera tarea de la lista por orden de insercion
            t.label must equalTo("Acabar la practica")
            dateIs(t.tiempo.get, "2013-12-25") must beTrue
            
          }
        }

    "Ascending order" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        // Creo una lista        
        val listaNormal: List[Task] = Task.ownTasks("josevi", Some(1))
        val t: Task = listaNormal.head        
        val Some(dueño) = Task.devuelveOwner2(t.id)
            
        // Compruebo el valor de la primera tarea de la lista que esta ordenada por fecha
        t.label must equalTo("Acabar la carrera")
        dateIs(t.tiempo.get, "2012-12-25") must beTrue
        dueño must equalTo("josevi")
      }
    }

  }
  
}